import {Http} from "@angular/http";
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';


@Injectable()
export class Database {

	endpoint: string;
	timeout: number;

	constructor(public http: Http) {
		this.endpoint = 'http://54.202.173.60/twitter/';
		this.timeout = 60000;
	}

	loadApp(appName: string, ok) {
		this.get(this.endpoint + 'get_app.php?app=' + appName, ok);
	}

	loadList(appName: string, ok) {
		this.get(this.endpoint + 'get_list.php?app=' + appName, ok);
	}

	getSentiment(query: string, ok) {
		this.get(this.getAnalysysURL('sentiment', query), ok);
	}

	getSubjectivity(query: string, ok) {
		this.get(this.getAnalysysURL('subjectivity', query), ok);
	}

	getSpam(query: string, ok) {
		this.get(this.getAnalysysURL('spam', query), ok);
	}

	getReadability(query: string, ok) {
		this.get(this.getAnalysysURL('readability', query), ok);
	}

	getLanguage(query: string, ok) {
		this.get(this.getAnalysysURL('language', query), ok);
	}

	getGender(query: string, ok) {
		this.get(this.getAnalysysURL('gender', query), ok);
	}

	getEducational(query: string, ok) {
		this.get(this.getAnalysysURL('educational', query), ok);
	}

	getCommercial(query: string, ok) {
		this.get(this.getAnalysysURL('commercial', query), ok);
	}

	getClassification(query: string, ok) {
		this.get(this.getAnalysysURL('classification', query), ok);
	}

	getAdult(query: string, ok) {
		this.get(this.getAnalysysURL('adult', query), ok);
	}

	get(url: string, ok) {
		this.http.get(url)
		.timeout(this.timeout)
		.map(res => res.json())
		.subscribe(ok, ()=>{});
	}

	getAnalysysURL(analysys: string, query: string) {
		return this.endpoint + analysys + '.php?query=' + query;
	}

}
