import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Database } from '../../modules/database';

export class Analysys {
    perc: string;
    label: string;

    constructor(perc, label) {
        this.perc  = perc;
        this.label = label;
    }
}

@Component({ templateUrl: 'item-details.html' })
export class ItemDetails {

    title: string;
    backgroundColor: string;

    sentiment: Analysys;
    subjectivity: Analysys;
    spam: Analysys;
    readability: Analysys;
    language: Analysys;
    gender: Analysys;
    educational: Analysys;
    commercial: Analysys;
    classification: Analysys;
    adult: Analysys;

    constructor(private params: NavParams,
                private viewCtrl: ViewController,
                private db: Database) {
        this.sentiment      = new Analysys('Carregando...', '');
        this.subjectivity   = new Analysys('Carregando...', '');
        this.spam           = new Analysys('Carregando...', '');
        this.readability    = new Analysys('Carregando...', '');
        this.language       = new Analysys('Carregando...', '');
        this.gender         = new Analysys('Carregando...', '');
        this.educational    = new Analysys('Carregando...', '');
        this.commercial     = new Analysys('Carregando...', '');
        this.classification = new Analysys('Carregando...', '');
        this.adult          = new Analysys('Carregando...', '');
    }

    ionViewDidLoad() {
        this.title = this.params.get("item");
        this.backgroundColor = this.params.get("color");

        this.db.getSentiment(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.sentiment = new Analysys(result[0], result[1]);
        });

        this.db.getSubjectivity(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.subjectivity = new Analysys(result[0], result[1]);
        });

        this.db.getSpam(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.spam = new Analysys(result[0], result[1]);
        });

        this.db.getReadability(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.readability = new Analysys(result[0], result[1]);
        });

        this.db.getLanguage(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.language = new Analysys(result[0], result[1]);
        });

        this.db.getGender(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.gender = new Analysys(result[0], result[1]);
        });

        this.db.getEducational(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.educational = new Analysys(result[0], result[1]);
        });

        this.db.getCommercial(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.commercial = new Analysys(result[0], result[1]);
        });

        this.db.getClassification(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.classification = new Analysys(result[0], result[1]);
        });

        this.db.getAdult(this.title, (analysys) => {
            let result = this.getGreaterItem(analysys);
            this.adult = new Analysys(result[0], result[1]);
        });

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    getGreaterItem(analysys) {
        let sum = 0;
        let maxVal = 0;
        let maxKey = '';
        for (let key in analysys) {
            sum += analysys[key];

            let val = Math.max(maxVal, analysys[key]);
            if (val > maxVal) {
                maxVal = val;
                maxKey = key;
            }
        }
        return [((maxVal/sum) * 100) + '%', maxKey];
    }
}
