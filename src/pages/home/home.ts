import { Component } from '@angular/core';
import { Database } from '../../modules/database';
import { ItemDetails } from '../../pages/item-details/item-details';
import { ModalController } from 'ionic-angular';

@Component({ templateUrl: 'home.html' })

export class HomePage {

    title: string;
    label: string;
    items: Array<string>;
    backgroundColor: string;
    appName: string;

    constructor(private db: Database, public modalCtrl: ModalController,) {
        this.appName = '<Nome do seu App, registrado em BD>'; // Ex: 'Filmes';

        this.title  = '';
        this.label  = '';
        this.backgroundColor = '';
        this.items = [];

        this.db.loadList(this.appName, (items) => {
            for(let item of items) {
                this.items.push(item);
            }
        });

        this.db.loadApp(this.appName, (app) => {
            for(let prop of app) {
                this.backgroundColor = prop[0];
                this.title           = prop[1];
                this.label           = prop[2];
            }
        });
    }

    onClick(item) {
        let info = this.modalCtrl.create(ItemDetails, { item: item, color: this.backgroundColor });
        info.present();
    }
}
